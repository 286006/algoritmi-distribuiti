var exec = require('child_process').exec;



var numNodes=8;
var value=[10,20,30,40,50,60,70,80];

for (let i = 0; i<numNodes; i++){
    if (i==0){
        pred=numNodes-1;
    }
    else{
        pred = i-1;
    }
    if (i==numNodes-1){
        succ = 0;
    }
    else{
        succ = i+1;
    }
    
    exec('node server.js '+i+' '+pred+' '+succ+' '+value[i]+' '+numNodes,
    function (error, stdout, stderr) {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
             console.log('exec error: ' + error);
        }
    });
}