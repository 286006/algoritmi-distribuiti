const express = require('express');
const app = express();
const bodyParser = require('body-parser');

module.exports = function (pred, succ, id, value, numNodes){
    
    const Ring = require('./api/models/ring');

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));


    let ringRoutes = require('./api/routes/ring')(pred,succ, id, value, numNodes);
    app.use('/ring', ringRoutes)

    app.use('/', express.static('public'));


    app.get('/', (req, res) => {
        res.status(200).send({
            message: "Id nodo: "+id+"; Id predecessore: "+pred+"; Id successore: "+succ+"; Valore nodo: "+value,
        })
    })
    return app;
};