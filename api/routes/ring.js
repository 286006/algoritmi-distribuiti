const express = require('express');
const { send } = require('express/lib/response');
const RingRoutes = express.Router();
const fileSystem = require('fs');
const path = require('path');

const Ring = require('../models/ring');



var leaderChanged,leaderSoFar;
var valueChanged, valueSoFar;
// 0 = non settato; 1 = leader; 2 = follower
var status = 0;
var filesRing = [];
module.exports = function (pred, succ, id, value, numNodes) {
    
    var ring = new Ring(id,value);
    leaderSoFar = id;
    //Il minimo visto fino ad ora sarà il mio valore
    valueSoFar = value;
    var nodes = [];

    //Getter per sapere se è leader, follower o non ancora definito.
    RingRoutes.route('/')
    .get(async function(req, res) {
        try{
            res.status(200);
            if(status == 0){
                res.json({message: 'Non sono ancora stato impostato'});
            }
            else if(status == 1){
                res.json({message: 'Sono il leader!'});
            }
            else{
                res.json({message: 'Sono un follower! Il leader è: '+leaderSoFar});
            }
			
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    });


    //Ho ricevuto un id dal predecessore, se è minore del mio oppure del minore che ho incontrato lo inoltro al successore.
    //Se invece è uguale al mio allora sono il leader e devo notificare tutti.
    RingRoutes.route('/election')
    .get(async function(req, res) {
        let sender = req.query.id;
        let valuesent = req.query.value;

        //Se il valore ricevuto è minore del valore che ho salvato come minimo allora è un nuovo minimo da salvare e inoltrare
        if(valuesent < valueSoFar){
            let leaderChanged = await ring.save(sender, valuesent);
            leaderSoFar = leaderChanged;
            valueSoFar = valuesent;
            ring.sendId(valueSoFar,leaderSoFar,succ,numNodes);
        }
        //Se è uguale al mio valore allora sono il leader e devo notificare tutti
        else if(valuesent == valueSoFar){
            status = 1;
            ring.sendNotification(id, numNodes);
            filesRing = await ring.getRingFiles(id, numNodes).then();
        }
        //Altrimenti il mio rimane il minimo e non serve inoltrarlo
        else{
            ring.sendId(valueSoFar,leaderSoFar,succ,numNodes);
            console.log("Leader non cambiato: id "+leaderSoFar+" con valore "+value);            
        }
        try{
            res.status(200);
			res.json({message: 'Id: '+id+' ; Value: '+value});
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    });

    //Ricevo una notifica, quindi sono un follower. Mi salvo chi è il leader
    RingRoutes.route('/notify/')
    .get(async function(req, res) {
        status = 2;
        try{
            res.status(200);
			res.json({message: 'Id: '+id+' ; Value: '+value});
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    });

    //Wake-up spontaneo, devo mandare il mio minimo al mio successore
    RingRoutes.route('/stabilize/')
    .get(async function(req, res) {
        if(status == 0){
          ring.sendId(valueSoFar,leaderSoFar,succ,numNodes);  
        }
        
        
        try{
            res.status(200);
			res.json({message: 'Sincronizzazione avvenuta con successo!'});
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    });

    //Restituisco la lista dei file che possiedo
    RingRoutes.route('/files/')
    .get(async function(req, res) {
        
        var files;
        files = await ring.getFiles(id);
        
        try{
            res.status(200);
			res.json({file: files});
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    });


    //Se non possiedo il file e non sono il leader restituisco errore, se possiedo il file lo restituisco mentre se non lo possiedo
    //e sono il leader restituisco l'id del nodo che lo possiede
    RingRoutes.route('/files/:file')
    .get(async function(req, res) {
        var fileRequested = req.params.file;
        var ringHasFile;
        var iHaveFile = await ring.doIHaveFile(fileRequested, id);
        if(iHaveFile == true){
            
            var filePath = path.join('./api/models/nodes/'+id, fileRequested);
            var stat = fileSystem.statSync(filePath);
            res.writeHead(200, {
                'Content-Type': 'txt',
                'Content-Length': stat.size
            });
            var readStream = fileSystem.createReadStream(filePath);
            readStream.pipe(res);
        }
        else if(status==1){
            ringHasFile = await ring.searchFile(fileRequested, filesRing, numNodes);
            if(ringHasFile == null){
                res.status(404);
			    res.json({message: 'Il file non esiste nel sistema'});
            }
            else{
               res.status(200);
			    res.json({message: 'Il file ce l\'ha l\'id '+ringHasFile}); 
            }
            
        }
        else{
            res.status(500);
			res.json({message: 'Non sono il leader e/o non possiedo il file!'});
        }
			
    });

    //Restituisco tutta la lista dei file del sistema, solo se sono il leader
    RingRoutes.route('/filesRing/')
    .get(async function(req, res) {
        
        if(status==1){
            // filesRing = await ring.getRingFiles(id, numNodes);
            res.status(200);
			res.json({message: filesRing});
        }
        else{
			res.status(500);
			res.json({message: 'Non sono il leader!'});
		}
    });

    //Crea file nella cartella del nodo
    RingRoutes.route('/createFile/')
    .get(async function(req, res) {
        try{
            ring.createFile(req.query.title, req.query.content, id);
            res.status(200);
			res.json({message: "File creato"});
        }
        catch{
			res.status(500);
			res.json({message: 'Non sono il leader!'});
		}
    });

    //Se sono il leader chiedo al nodo di creare il file detto dall'utente. Prende come parametri id, title e content
    RingRoutes.route('/requestCreation/')
    .get(async function(req, res) {

        if(status==1){
            if(await ring.searchFileInNode(req.query.title, filesRing, req.query.id)==false){
                ring.requestCreation(req.query.id, req.query.title, req.query.content)
                filesRing = await ring.getRingFiles(id, numNodes).then();
                res.status(200);
                res.json({message: "File creato"});
            }
            else{
                res.status(400);
                res.json({message: "Questo nodo ha già un file con quel nome!"});
            }
            
        }
        else{
			res.status(500);
			res.json({message: 'Non sono il leader!'});
		}
    });

    //Rinominare un file
    RingRoutes.route('/renameFile/')
    .get(async function(req, res) {
        try{
            ring.renameFile(req.query.titleold, req.query.titlenew, id);
            res.status(200);
			res.json({message: "File rinominato"});
        }
        catch{
			res.status(500);
			res.json({message: 'Non sono il leader!'});
		}
    });

    //Se sono il leader chiedo al nodo di modificare il file detto dall'utente. Prende come parametri id, titleold e titlenew
    RingRoutes.route('/requestEdit/')
    .get(async function(req, res) {

        if(status==1){
            if(await ring.searchFileInNode(req.query.titleold, filesRing, req.query.id)==true){
                if(await ring.searchFileInNode(req.query.titlenew, filesRing, req.query.id)==false){
                    ring.requestRename(req.query.id, req.query.titleold, req.query.titlenew)
                    filesRing = await ring.getRingFiles(id, numNodes).then();
                    res.status(200);
                    res.json({message: "File modificato con successo!"});
                }
                else{
                    res.status(400);
                    res.json({message: "Questo nodo ha già un file con quel nome!"});
                }
            }
            else{
                res.status(400);
                    res.json({message: "Questo nodo non ha un file con quel nome!"});
            }   
        }
        else{
			res.status(500);
			res.json({message: 'Non sono il leader!'});
		}
    });

    return RingRoutes;
}

