var uniqid = require('uniqid');
const http = require('node:http');
const axios = require('axios');
const fs = require('fs');
const { formatWithOptions } = require('node:util');
const { CONNREFUSED } = require('node:dns');

var minValue = 1000;
var leader = 0;
var numReceived = 0;

class Ring {

    async save(id, value) {
      minValue = value;
      leader = id
      numReceived++;

      return leader;
    }

    //Funzione che manda il minimo al successore
    async sendId(value, id, dest, numNodes) {
          axios.get('http://localhost:5000'+dest+'/ring/election',{
            params: {
            id: encodeURI(id),
            value: encodeURI(value)
          }});
      }

      async receivedGetter(){
        return numReceived;
      }
    
      //Funzione che manda la notifica a tutti i nodi
      async sendNotification(id, numNodes){
        for(let i=0; i<numNodes; i++){
          if(i!=id){
            axios.get('http://localhost:5000'+i+'/ring/notify',{
              params: {
              id: encodeURI(id)
            }});  
          }
        }
      }

      //Funzione che restituisce la lista dei file del nodo
      async getFiles(id){
        var filesDir=[];
        fs.readdirSync('./api/models/nodes/'+id+'/').forEach(file => {
          filesDir.push(file);
        });
        return filesDir;
      }

      //Funzione che ottiene la lista dei file del sistema
      async getRingFiles(id, numNodes){
        var filesDir=[];
        for(let i=0; i<numNodes; i++){
            axios.get('http://localhost:5000'+i+'/ring/files',{}).then(resp => {
              var string = JSON.stringify(resp.data);
              var objectValue = JSON.parse(string);
              
              filesDir[i]=objectValue['file'];
          });
        }
        return filesDir;
      }

      //Funzione che controlla se possiedo il file
      async doIHaveFile(fileSearched, id){
        var haveFile = false;
        fs.readdirSync('./api/models/nodes/'+id+'/').forEach(file => {
          if(file == fileSearched){
            haveFile=true;
          }
        });
        return haveFile;
      }


      //Funzione che cerca in quale nodo è presente il file richiesto e, se non presente, restituisce null
      async searchFile(file, filesList, numNodes){
        var nodeHasFile = null;
        for(let i=0; i<numNodes; i++){
          filesList[i].forEach(element => {
              if (element == file){
                nodeHasFile = i;
              }
            });
        }
        return nodeHasFile;
      }

      //Funzione che cerca se il nodo possiede il file
      async searchFileInNode(file, filesList, id){
        var nodeHasFile = false;
        filesList[id].forEach(element => {
          if (element == file){
            nodeHasFile = true;
          }
        });
        return nodeHasFile;
      }

      //Funzione chiamata dal leader che richiede la creazione del file al nodo
      async requestCreation(id, title, body){
        axios.get('http://localhost:5000'+id+'/ring/createFile',{
          params: {
          title: encodeURI(title),
          content: encodeURI(body)
        }});
      }

      //Funzione che crea il file
      async createFile(title, body, id){
        fs.writeFileSync('./api/models/nodes/'+id+'/'+title, body);
      }

      //Funzione chiamata dal leader che richiede la modifica del nome del file al nodo
      async requestRename(id, titleold, titlenew){
        axios.get('http://localhost:5000'+id+'/ring/renameFile',{
          params: {
          titleold: encodeURI(titleold),
          titlenew: encodeURI(titlenew)
        }});
      }

      //Funzione che rinomina il file
      async renameFile(titleold, titlenew, id){
        fs.renameSync('./api/models/nodes/'+id+'/'+titleold, './api/models/nodes/'+id+'/'+titlenew);
      }
      
};



module.exports = Ring;